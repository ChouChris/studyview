from django.shortcuts import render, HttpResponse, redirect
import json
from django.http.response import JsonResponse, FileResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required


def demo (request):
    return HttpResponse('123456789')

def index(request):
    return render(request,'index.html')

def render_two(request):
    one = '1243t443w6'
    two='esrdgfgtehfg'
    # 傳參:透過render函數，將後端職渲染到前端，無個數限制

    #狀態碼:200(成功)   404(找不到)   500(服務器內部錯誤) 屬於規範 可以不遵循
    return render(request, 'detail.html', {'one': one, 'two': two}, status=403)

def json_response(request):
    data_dict = {
        '123': '456',
        'abc': 'def'
    }
    data_dict_json_str = json.dumps(data_dict)
    return HttpResponse(data_dict_json_str)

def json_response2(request):
    data_dict = {
        '123': '456',
        'abc': 'def'
    }
    return JsonResponse(data_dict)

#下載-----------------------------------------------------
def download_file(request):
    file = open('img1.jpg', 'rb')
    # response = HttpResponse(content=file.read(), content_type='image/jepg') # 展示圖片
    response = HttpResponse(content=file.read(), content_type='application/x-jpg') # 下載圖片
    response['Content-Disposition'] = "attachment;filename*=utf8'' {}".format('penguin.jpg')
   #  file.read()ˊ只適合小文件
    # response = HttpResponse(content=file.read(), content_type='application/x-msdownload') # 下載圖片
    # response['Content-Disposition'] = "attachment;filename*=utf8'' {}".format('python-test.exe')
    
    return response

def stream_download_file(request):
    #在內存中開闢4096個字節空間，從文件中多次讀取這麼多數據然後返回，再讀取再返回
    # 缺點: 需要長時間的維持一個連結，需要消耗資源
    return FileResponse(open('pythonw.exe', 'rb'), as_attachment=True, filename='python-test.exe')



# 提取wsgi中的數據---------------------------------------------------
def get_request_info(request):
    print(request.path)
    print(request.method)
    print(request.GET, request.POST, request.FILES, request.COOKIES, request.session)
    print(request.user, request.user.is_authenticated)
    print(request.META)
    if request.META.get('HTTP_X_FORWARDED_FOR'):
        ipaddr = request.META.get('HTTP_X_FORWARDED_FOR')
    else:
        ipaddr = request.META.get('REMOTE_ADDR')
    print('真實ip地址是: {}'.format(ipaddr))
    print(request.is_secure())
    # print(request.is_ajax())  ??
    print(request.get_host())
    print(request.get_full_path())
    # print(request.get_raw_uri()) ??
    return HttpResponse('''<h1> get request info </h1>
        <h1>用戶信息: {} {}</h1>
        <h1>真實ip地址: {}</h1>
'''.format(request.user, request.user.is_authenticated, ipaddr))

# 提取请求中Cookie和Get的参数值--------------------------------------
def query_dict_get_cookies(request):
    print(request.COOKIES.get('csrftoken'))
    print(request.COOKIES.get('sessionid'))
    print(request.GET.get('demo')) # 網域/?demo=3456y5&rrr=452sgdf
    print(request.GET.get('rrr'))
    return HttpResponse('success')

# 獲得POST請求參數和上傳文件---------------------------------------
def query_dict_post_files(request):
    print(request.POST.get('ko'))      # 搭配postman使用，body的form-data使用
    print(request.POST.getlist('ko'))   # getlist得到列表
    print(request.FILES.get('jpg'))
    print(request.POST)
    print(request.POST.get('de'))    # postman的x-www-form-urlencoded
    print(request.POST.get('fr'))
    print(request.POST.get('gt'))

    # 文件保存在服務器
    # file = request.FILES.get('jpg')
    # print(file)
    # with open(file.name, 'wb') as f:
    #     f.write(file.read())

    #獲取請求主體的內容
    # data = request.body           #postman的raw傳輸任意格式的文本
    # data_dict = json.loads(data)  # 若傳來不是json格式會報錯
    # print(data)
    # print(data_dict)
    print("-----------")

 
    file = request.body           # binary傳輸文件
    with open('img1.jpg', 'wb') as f:
        f.write(file)

    return HttpResponse('post files success')
#postman
# form-data多部分表格數據，可以傳輸文字和文件
# x-www-form-urlencoded僅可以傳輸表單，不能傳輸文件
# raw可以傳輸任意格式的字串符
# binary只傳輸文件(檔案)


# 重新定向的使用場景--------------------------------------------------
# 跳轉url的格式:   https://host/url?跳轉參數名=源url   購物車沒登入跳轉到登入頁等等 (場景1)

# get: 有長度限制，可以被瀏覽器收藏
# post: 沒有長度限制，不可被瀏覽器收藏
# 瀏覽器的刷新按鈕，是重新發送上一次請求
#(場景2) 使用post登入，然後返回頁面，點擊瀏覽器刷新按鈕，則會再次發送post請求，這個是可以實現的，但千萬不要給post返回頁面(會被批量post)，而是返回跳轉

# 重新定向的HttpResponseRedirect-------------------------------------------------------------
from django.http.response import HttpResponseRedirect

def login_success_view(request):
    #登入檢測，未登入跳轉
    if request.user:       # 沒登入也會有匿名使用者
        print('{} 通過'.format(request.user))
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/unlogin_view/') # 括號內放跳轉url
    #登入成功展示響應結果
    return HttpResponse('當前已經登入: {}'.format(request.user.username))

def unlogin_view(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/login_success_view/')
    return HttpResponse('當前是未登入狀態')

#----------redirect使用方式-------------------------
from django.shortcuts import redirect

def redirect_demo_param(request, id):
    return HttpResponse('當前id值是: {}'. format(id))

def ready_to_redirect(request):
    # return HttpResponseRedirect('/redirect_demo_param/8/')
    # return redirect(to='/redirect_demo_param/7/')
    return redirect(to='rdp', id=3)


## 裝飾器場景的應用-----(登入檢測)----------------------------------
from django.contrib.auth.decorators import login_required

# @login_required  # setting.py可設定login_url來跳轉未登入時的頁面
# @login_required(login_url='/demosssssssss/')
# @login_required(login_url='gri')
# @login_required(login_url='rdp') # 報錯
# @login_required(login_url='rdp', id=3) # 報錯
@login_required(login_url='redirect_demo_param/18/')
def test_check_login_view(request):
    return HttpResponse('already login')

# ----------FBV與CBV差別----------------------------------------------
# def函數+url的形式叫做fbv
# class類名+url形式叫做cbv，類可以繼承，可以拓展，django還有一種功能類，叫做mixin
