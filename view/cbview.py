from django.views import View
from django.shortcuts import HttpResponse
from django.views.generic import TemplateView, RedirectView

# 在 Django 中，有兩種主要方式來建立視圖：基於類的視圖（CBV）和基於函數的視圖（FBV）。CBV 利用類來封裝視圖的邏輯和數據，
# 使得代碼更加結構化和可重用；而 FBV 則直接使用函數來處理請求，對於簡單的情況或快速開發來說，可能更直接和簡單。CBV 提供更多內建
# 功能和擴展性，而 FBV 則可能對於某些簡單或特定需求的實現來說更加直觀。选择哪一种取决于个人偏好和项目需求

class DemoCBView(View):
    def get(self,request):
        return HttpResponse('這是get請求，所以返回get函數的HttpResponse')
    
    def post(self, request):
        return HttpResponse('這是post請求，所以返回post函數的HttpResponse')
    
    def put(self, request):
        return HttpResponse('這是put請求，所以返回put函數的HttpResponse')



def demofbview(request):
    if request.method.lower() == 'get':
        return HttpResponse('demofbview這是get請求，所以返回get函數的HttpResponse')
    if request.method.lower() == 'post':
        return HttpResponse('demofbview這是get請求，所以返回get函數的HttpResponse')
    if request.method.lower() == 'put':
        return HttpResponse('demofbview這是get請求，所以返回get函數的HttpResponse')
    


class DetailView(TemplateView):               # 多看看文件叫好理解...........................................................
    # TemplateView = TemplateResponesMixin + ContextMixin + View
    template_name = 'detail.html'

    def get_context_data(self, **kwargs):
        # get -> get_context_data -> 父級的get_context_data -> get_context_data -> get ->   response
        context = super().get_context_data(**kwargs)
        context['one'] = '1233435678970'
        context['two'] = 'refajgbnkjbnf;kwe'
        return context

class TargetView(RedirectView):
    permanent = False
    url = None
    pattern_name = 'gri' # 指定url裡面的name
    query_string = True

    def get_redirect_url(self, *args, **kwargs):
        #做我們需額外加的操作，如更新數據庫操作
        pass
        return super().get_redirect_url(*args, **kwargs) # 調用父級的get_redirect_url函數
    