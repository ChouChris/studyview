"""
URL configuration for studyview project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from view.views import demo, index, render_two, json_response, json_response2, download_file,stream_download_file, get_request_info, query_dict_get_cookies
from view.views import query_dict_get_cookies,query_dict_post_files
from view.views import login_success_view,unlogin_view
from view.views import redirect_demo_param,ready_to_redirect,test_check_login_view
from view.cbview import DemoCBView, demofbview, DetailView, TargetView
from django.views.generic import TemplateView, RedirectView

urlpatterns = [
    path("admin/", admin.site.urls),
    path('demo/', demo),
    path('index/', index),
    path('render_two/', render_two),
    path('json_response/', json_response),
    path('json_response2/', json_response2),
    path('download_file/', download_file),
    path('stream_download_file/', stream_download_file),
    path('get_request_info/', get_request_info, name='gri'),
    path('query_dict_get_cookies/', query_dict_get_cookies),
    path('query_dict_post_files/', query_dict_post_files),
    path('login_success_view/', login_success_view),
    path('unlogin_view/', unlogin_view),
    path('redirect_demo_param/<int:id>/', redirect_demo_param, name='rdp'),
    path('ready_to_redirect/', ready_to_redirect),
    path('test_check_login_view/', test_check_login_view),
]

urlpatterns += [
    path('DemoCBView/', DemoCBView.as_view()), # class 要.as_view() 
    path('demofbview/', demofbview),
    path('templateview/index/', TemplateView.as_view(template_name='index.html')),
    path('templateview/detail/', TemplateView.as_view(template_name='detail.html')),
    path('templateview/detail2/', DetailView.as_view()),
    path('redirectview/123/', RedirectView.as_view(url='/index/')),
    path('redirectview/123456/', TargetView.as_view()),
]